import random

class Tree:
    fruit_type = ''
    is_on_tree = True

    def __init__(self, fruit_type):
        self.fruit_type = fruit_type

    def show(self):
        f = self.fruit_type
        code = f'''
　　　　／＼
　　　／　　＼
　　／　　{f}　＼
　＜＿{f}＿＿{f}＿＞
　　　　|　|
＿＿＿＿|＿|＿＿＿＿
'''

        if not self.is_on_tree:
            b = self.fall_bee()
            code = f'''
　　　　／＼
　　　／　　＼
　　／　　　　＼
　＜＿＿＿＿＿＿＞
　　　　|　|
＿＿{b}{f}|＿|{f}{f}＿＿
'''

        print(code + "\033[7A", end='')

    def fall_fruits(self):
        self.is_on_tree = False

    def fall_bee(self):
        return '🐝' if random.random() <= 1 / 3 else '＿'

    def reset_fruits(self):
        self.is_on_tree = True

def main():
    tree = Tree('🍎')
    while True:
        tree.show()
        key = input()
        if key == '1':
            tree.fall_fruits()
        else:
            tree.reset_fruits()

        print("\033[1A", end='')

if __name__ == '__main__':
    main()